%title: KEYCLOAK
%author: xavki


# KEYCLOAK : Les MODES

<br>

Standalone mode :

		* pas en production

		* juste une instance

		* pas de partage des caches

		* test ou dev environnement

		* fichier principal de configuration standalone.xml

```
./bin/standalone.sh
```

----------------------------------------------------------------------------------------

# KEYCLOAK : Les MODES

<br>

Standalone Clustered mode :

		* production ready

		* cluster avec keycloak sur chaque machine

		* besoin de configurer chaque serveur du cluster (automatisation à faire)

		* fichier principal de configuration standalone-ha.xml

```
./bin/standalone.sh --server-config=standalone-ha.xml
```

----------------------------------------------------------------------------------------

# KEYCLOAK : Les MODES

<br>

Domain Clustered mode :

		* centralisation de la configuration

		* publication centralisée

		* via WildFly (serveur d'application)

		* domain controller : point central pour stocker et manager les confs

		* host controller : en charge des instances de son serveur
			(échange avec le domain controller)

		* domain profile : configuration d'un domaine appliqué sur les serveurs cibles
			(un domain contoller peut en gérer plusieurs)

----------------------------------------------------------------------------------------

# KEYCLOAK : Les MODES

<br>

Cross-site Replication Mode :

		* multiple clusters keycloak

		* réplication entre les clusters

		* exemple : différentes région ou DC
