%title: KEYCLOAK
%author: xavki


# KEYCLOAK : version 17 - mode dev

<br>

* installation 

```
wget https://github.com/keycloak/keycloak/releases/download/17.0.0/keycloak-17.0.0.tar.gz
```

<br>

* premier lancement

```
export KEYCLOAK_ADMIN=admin
export KEYCLOAK_ADMIN_PASSWORD=admin
```

```
./kc.sh start-dev
```

-------------------------------------------------------------------------------------

# KEYCLOAK : version 17 - mode dev

<br>

* initialisation des creds

```
ls -la ~/.keycloak/kcadm.config
./kcadm.sh config credentials --server http://localhost:8080/ --realm master --user admin --password admin
ls -la ~/.keycloak/kcadm.config
```

<br>

* création d'un user supplémentaire

```
./kcadm.sh create users -r master -s username=xavki -s enabled=true
./kcadm.sh set-password --username xavki -p
```
