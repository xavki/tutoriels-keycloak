%title: KEYCLOAK
%author: xavki


# KEYCLOAK : Première installation & Directories

<br>

Standalone mode :

		* pas en production

		* juste une instance

		* pas de partage des caches

		* test ou dev environnement

		* fichier principal de configuration standalone.xml

```
./bin/standalone.sh
```

---------------------------------------------------------------------------------------------

# KEYCLOAK : Première installation & Directories


<br>


bin/

		* les scripts et binaires pour manager et lancer keycloak

<br>

domain/

		* configuration et répertoires relatifs au domain mode

<br>

modules/

		* les librairies java (exemple : mariadb)

<br>

standalone/

		* fichiers de configurations dédiés au standalone mode

<br>

standalone/deployments/

		* espace dédié aux extensions de keycloak

<br>

themes/

		* dédié aux statiques des thèmes d'authentification 

---------------------------------------------------------------------------------------------

# KEYCLOAK : Première installation & Directories


<br>

```
openjdk-17-jre-headless

wget https://github.com/keycloak/keycloak/releases/download/16.1.1/keycloak-16.1.1.tar.gz

tar xzvf keycloak-16.1.1.tar.gz

./bin/standalone.sh -c=standalone-ha.xml -b=192.168.14.10 -bmanagement=192.168.14.10
```

<br>

* création du user d'admin

```
root@node1:/opt/keycloak/bin# ./add-user-keycloak.sh --user admin --password xavki
```
