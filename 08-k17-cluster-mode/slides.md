%title: KEYCLOAK
%author: xavki


# KEYCLOAK : version 17 - cluster

<br>

* installation 

```
apt install openjdk-17-jre-headless docker.io
wget https://github.com/keycloak/keycloak/releases/download/17.0.0/keycloak-17.0.0.tar.gz
```

<br>

* lancement d'une base de données

```
docker run -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=keycloak -e POSTGRES_USER=postgres -d -p 5432:5432 postgres
```

--------------------------------------------------------------------------------------

# KEYCLOAK : version 17 - cluster


<br>

* modification de la configuration infinispan

<br>

* récupération de :

		* la configuration

		* le keystore

		* et la configuration infinispan


--------------------------------------------------------------------------------------

# KEYCLOAK : version 17 - cluster


<br>

* rebuild

```
./kc.sh build --db=postgres --cache-config-file=cache-ispn.xml
```


```
frontend test
  bind *:443
  mode tcp
  use_backend xavki
backend xavki
  mode tcp
  balance roundrobin
  option forwardfor
  server node1 192.168.14.10:443 check
  server node2 192.168.14.11:443 check
```
