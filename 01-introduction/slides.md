%title: KEYCLOAK
%author: xavki


# KEYCLOAK : Introduction

<br>

KEYCLOAK = Identification and Access Management (IAM)

<br>

Site : https://www.keycloak.org/
Dépôt : https://github.com/keycloak/keycloak

<br>

Licence Apache

<br>

Sponsorisé par RedHat > Redhat SSO

--------------------------------------------------------------------------------

# KEYCLOAK : Introduction


<br>

Le besoin ?

<br>

		* disposer d'un outil centralisé (SSO)

<br>

		* fédérer les identités

<br>

		* pour l'authentification / authorization

<br>

		* permettant de simplifier la gestion (via console)

<br>

		* utilisation des standards : OpenID Connect (surcouche OAuth 2.0) ou SAML

--------------------------------------------------------------------------------

# KEYCLOAK : Introduction

<br>

RFC :

	* OAuth 2.0 : https://datatracker.ietf.org/doc/html/rfc6749

	* SAML 2.0 : https://datatracker.ietf.org/doc/html/rfc7522

	* OpenID connect : https://openid.net/specs/openid-connect-core-1_0.html


OpenID Connect = Identity + Authentication + Authorization

--------------------------------------------------------------------------------

# KEYCLOAK : Introduction


<br>

Uilisable :

<br>

		* sécurisation d'interfaces graphiques

<br>

		* sécurisation d'API

<br>

		* Single Sing On (SSO)

<br>

		* mise à disposition de librairies dans de nombreux langages

<br>

		* intégration des logins sociaux (google, github, facebook...)

<br>

		* intégration d'annuaires LDAP ou Active Directory

--------------------------------------------------------------------------------

# KEYCLOAK : Introduction


<br>

Possibilité de déployer sur :

<br>

		* VM ou machine physique

<br>

		* Conteneurs et Kubernetes (operator)

<br>

		* standalone ou cluster

<br>

		* Java / Jboss

<br>

		* haute performance

<br>

		* base de données local (h2) ou déportée (mariadb/mysql, postgres...)
