%title: KEYCLOAK
%author: xavki


# KEYCLOAK : Définitions & Concepts

<br>

Users

	* entités pouvant se loguer

	* associé à des attibuts : email, username...

	* associé à un groupe et/ou des rôles

<br>

Groups

	* groupe d'utilisateurs

	* les utilisateurs héritent des attributs du groupe

	* les utilisateurs héritent des rôles du groupe

	* facilité de gestion de nombreux utilisateurs

------------------------------------------------------------------------------------

# KEYCLOAK : Définitions & Concepts

<br>

Roles 

	* niveau de droits ou rôles d'utilisateurs

	* exemple : admin

	* une application utilise le rôle comme niveau de permissions

<br>

Realms

	* ensemble de users, creds, roles et groupes

	* niveau d'isolation (pas d'échanges entre les realms)

------------------------------------------------------------------------------------

# KEYCLOAK : Définitions & Concepts

<br>

Clients

	* outil cherchant à identifier des users

	* exemple : une application

Clients Roles

	* rôle spécifiques aux clients

------------------------------------------------------------------------------------

# KEYCLOAK : Définitions & Concepts

<br>

Client adaptater

	* élément de l'outil permettant la connexion avec keycloak

Consent (consentement)

	* autorisation d'un user pour un client

	* après login

------------------------------------------------------------------------------------

# KEYCLOAK : Définitions & Concepts

<br>

Access Token

	* partie d'une requête HTTP

	* autorise l'accès au service

	* header

<br>

Identity Token

	* token fournissant des informations d'utilisateurs

	* spécifications de OpenID Connect

------------------------------------------------------------------------------------

# KEYCLOAK : Définitions & Concepts

<br>

User Federation Provider

	* connexion à un stockage local externe (AD ou LDAP)

<br>

Identity Provider Federation

	* utilisation d'une identification externe

	* réseaux sociaux etc

	* via protocoles OpneID Connect ou SAML 2.0

