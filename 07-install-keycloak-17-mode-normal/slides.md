%title: KEYCLOAK
%author: xavki


# KEYCLOAK : version 17 - mode normal

<br>

* installation 

```
apt install openjdk-17-jre-headless docker.io
wget https://github.com/keycloak/keycloak/releases/download/17.0.0/keycloak-17.0.0.tar.gz
```

<br>

* lancement d'une base de données

```
docker run -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=keycloak -e POSTGRES_USER=postgres -d -p 5432:5432 postgres
```

--------------------------------------------------------------------------------------

# KEYCLOAK : version 17 - mode normal


<br>

* génération du keystore pour le cert lts

```
keytool -genkeypair -alias localhost -keyalg RSA -keysize 2048 -validity 365 \
-keystore server.keystore -dname "cn=Server Administrator,o=Acme,c=GB" -keypass secret -storepass secret
mv /home/vagrant/server.keystore  conf/
```

--------------------------------------------------------------------------------------

# KEYCLOAK : version 17 - mode normal


<br>

* premier lancement

```
export KEYCLOAK_ADMIN=admin
export KEYCLOAK_ADMIN_PASSWORD=admin
```

<br>

* build 

```
./kc.sh build --db postgres
```

--------------------------------------------------------------------------------------

# KEYCLOAK : version 17 - mode normal

<br>

* configuration

```
db-url-host=localhost
db-username=postgres
db-password=postgres
hostname=xavki.keycloak
https-port=443
https-key-store-password=secret
```

<br>

* lancement avec fichier de conf

```
./kc.sh start --https-key-store-password=secret
```

* lancement sans fichier de conf

```
./kc.sh start --db-url-host localhost --db-username postgres \
 --db-password postgres --hostname=fedora --https-key-store-password=secret
```
